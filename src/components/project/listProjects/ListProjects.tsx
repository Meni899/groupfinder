import { useEffect, useReducer } from "react";
import { Project } from "../../../types/projects";
import { ProjectService } from "../../../firebase";
import { Link } from "react-router-dom";
import global from "../../../styles/global.module.scss";
import scss from "./listprojects.module.scss";
import presentation from "../../../assets/presentation.svg";
import ProjectCard from "../projectCard/ProjectCard";

const reducer = (projects: Project[], action: Project | undefined) => {
  if (action === undefined) {
    return [];
  } else {
    return [...projects, action];
  }
};

const ListProjects = () => {
  const [projects, dispatch] = useReducer(reducer, []);

  useEffect(() => {
    const projectCollection = ProjectService.getProjects()
      .orderBy("timestamp")
      .onSnapshot(
        (snapshot) => {
          dispatch(undefined);
          snapshot.docs.forEach((doc) => {
            dispatch({
              id: doc.id,
              ...doc.data(),
            } as Project);
          });
        },
        (error) => {
          console.log("Error getting documents:", error);
        }
      );
    return () => {
      projectCollection();
    };
  }, []);

  return (
    <section className={global.fullHeightCentered}>
      <div className={scss.titleBlock}>
        <img
          src={presentation}
          alt='Presentation icon'
          className={scss.presentationIcon}
        />
        <div>
          <h1 className={global.titleBig}>Project Overview</h1>
          <p>
            All available Projects are listed here, choose one to find a group
            for that Project.
          </p>
        </div>
      </div>
      <Link
        to={"project/createProject"}
        className={scss.createButton}
        data-testid='createProjectButton'
      >
        <button className={global.buttonGreen}>Create Project</button>
      </Link>
      <section className={scss.projectList}>
        {projects.map((project, index) => (
          <ProjectCard project={project} key={index}></ProjectCard>
        ))}
      </section>
    </section>
  );
};

export default ListProjects;
