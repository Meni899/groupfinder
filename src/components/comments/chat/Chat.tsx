import { useEffect, useReducer } from "react";
import { ProjectService } from "../../../firebase";
import { Comment } from "../../../types/comments";
import { useParams } from "react-router-dom";
import { User } from "../../../types/user";
import { CommentProps } from "../../../types/commentProps";
import scss from "./chat.module.scss";
import likeOrange from "../../../assets/like-orange.svg";
import likeBlue from "../../../assets/like-blue.svg";
import store from "../../store/store";
import { ProjectIDParam, TopicIDParam } from "../../../types/paramObjs";

const reducer = (comments: Comment[], action: Comment | undefined) => {
  if (action === undefined) {
    return [];
  } else {
    return [...comments, action];
  }
};

export const Chat = ({ topic }: CommentProps) => {
  const { pid }: ProjectIDParam = useParams();
  const { tid }: TopicIDParam = useParams();
  const [comments, dispatch] = useReducer(reducer, []);

  const user: User = {
    uid: store.getState().user.uid,
    email: store.getState().user.email,
    displayName: store.getState().user.displayName
  };

  useEffect(() => {
    const commentCollection = ProjectService.getProjectById(pid)
      .collection("topics")
      .doc(tid)
      .collection("comments")
      .orderBy("timestamp", "asc")
      .onSnapshot((snapshot) => {
        dispatch(undefined);
        snapshot.docs.forEach((doc) => {
          dispatch({
            id: doc.id,
            ...doc.data()
          } as Comment);
        });
      });
    return () => {
      commentCollection();
    };
  }, [pid, tid]);

  const handleLikeComment = async (comment: Comment) => {
    const isAlreadyLikedByUser: boolean =
      comment.likes.find((likeUser) => user.uid === likeUser.uid) !== undefined;
    if (!isAlreadyLikedByUser) {
      ProjectService.getProjectById(pid)
        .collection("topics")
        .doc(tid)
        .collection("comments")
        .doc(comment.id)
        .set({ likes: [...comment.likes, user] }, { merge: true })
        .catch(() => console.log("Something went wrong"));
    } else {
      comment.likes.splice(comment.likes.indexOf(store.getState().user), 1);
      ProjectService.getProjectById(pid)
        .collection("topics")
        .doc(tid)
        .collection("comments")
        .doc(comment.id)
        .update({ likes: comment.likes })
        .catch(() => console.log("Something went wrong"));
    }
  };

  return (
    <ul className={scss.commentContainer}>
      {comments.map((comment, index) => (
        <li key={index} className={scss.commentItem}>
          <div>
            <div className={scss.userInfo}>{comment.user.displayName}</div>
            <div>{comment.message}</div>
          </div>
          <div className={scss.likeContainer}>
            <span>
              {comment.likes &&
                comment.likes.length > 0 &&
                comment.likes.length}
            </span>
            <button className={scss.likeButton}>
              <img
                src={
                  comment.likes && comment.likes.length > 0
                    ? likeOrange
                    : likeBlue
                }
                alt="like"
                className={scss.heartIcon}
                onClick={() => {
                  if (!topic.resolved) handleLikeComment(comment);
                }}
              />
            </button>
          </div>
        </li>
      ))}
    </ul>
  );
};
