import { User } from "../../types/user";

export type UserState = {
  user: User | undefined;
};

const initialUserState = {
  user: undefined,
};

export type TodoAction = {
  type: "SET_USER";
  payload: User;
};

const userReducer = (
  user: UserState = initialUserState,
  action: TodoAction
) => {
  switch (action.type) {
    case "SET_USER":
      return {
        user: action.payload as User,
      };
  }
};

export default userReducer;
