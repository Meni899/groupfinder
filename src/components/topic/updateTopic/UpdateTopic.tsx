import { useState, useEffect } from "react";
import { Topic } from "../../../types/topics";
import { ProjectService } from "../../../firebase";
import { Link, useParams } from "react-router-dom";
import { Error } from "../../../types/error";
import { useHistory } from "react-router-dom";
import global from "../../../styles/global.module.scss";
import scss from "./updatetopic.module.scss";
import { ProjectIDParam, TopicIDParam } from "../../../types/paramObjs";

const UpdateTopic = () => {
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [maxMembers, setMaxMembers] = useState(1);
  const [errors, setErrors] = useState<Error[]>([]);
  const [topic, setTopic] = useState<Topic | undefined>(undefined);
  const history = useHistory();

  const { pid }: ProjectIDParam = useParams();
  const { tid }: TopicIDParam = useParams();

  useEffect(() => {
    const topic = ProjectService.getProjectById(pid)
      .collection("topics")
      .doc(tid)
      .onSnapshot(
        (snapshot) => {
          setTopic(snapshot.data() as Topic);
          setName(snapshot.data()?.name);
          setDescription(snapshot.data()?.description);
          setMaxMembers(snapshot.data()?.maxMembers);
        },
        (error) => {
          console.log("Error getting documents:", error);
        }
      );

    return () => {
      topic();
    };
  }, [pid, tid]);

  const clearErrors = () => {
    setErrors([]);
  };

  const handleUpdateTopic = () => {
    clearErrors();
    const topicMembers = topic?.members ? topic?.members.length : 0;
    if (name === "" || maxMembers < 0 || maxMembers < topicMembers) {
      if (name === "") {
        setErrors((errors) => [...errors, { msg: "name is required" }]);
      }
      if (maxMembers < 0) {
        setErrors((errors) => [
          ...errors,
          { msg: "maximum number of members has to be 0 or greater" }
        ]);
      }
      if (maxMembers < topicMembers) {
        setErrors((errors) => [
          ...errors,
          {
            msg:
              "there are already " +
              topicMembers +
              " members participating in your project so maximum members cant be lower"
          }
        ]);
      }
    } else if (topic) {
      topic.name = name;
      topic.description = description;
      topic.maxMembers = maxMembers;
      ProjectService.getProjectById(pid)
        .collection("topics")
        .doc(tid)
        .update(topic)
        .then((docRef) => {
          console.log("Document updated with ID: ", docRef);
          history.push("/project/" + pid);
        })
        .catch((error) => {
          console.error("Error adding document: ", error);
        });
    }
  };
  return (
    <section>
      <div className={global.fullHeightCentered}>
        <h1 className={global.titleBig}>Update Topic</h1>
        <div className={global.errorMsg}>
          {errors.map((error, index) => (
            <span key={index}>{error.msg} </span>
          ))}
        </div>
        <input
          type="text"
          autoFocus
          required
          value={name}
          onChange={(e) => setName(e.target.value)}
          className={global.inputfield}
          placeholder="Topic Name"
          aria-label="Topic Name"
          data-testid="topicName"
        />
        <input
          type="text"
          autoFocus
          required
          value={description}
          onChange={(e) => setDescription(e.target.value)}
          className={global.inputfield}
          placeholder="Description"
          aria-label="Description"
        />
        <input
          type="number"
          autoFocus
          required
          value={maxMembers}
          onChange={(e) => setMaxMembers(e.target.valueAsNumber)}
          className={global.inputfield}
          placeholder="Max. Group Members"
          aria-label="Max. Group Members"
        />

        <button
          onClick={handleUpdateTopic}
          className={`${global.buttonGreen} ${scss.submitButton}`}
          data-testid="updateButton"
        >
          Update Topic
        </button>
        <Link to={"/project/" + pid}>
          <button className={`${global.buttonOrange} ${scss.backButton}`}>
            Cancel
          </button>
        </Link>
      </div>
    </section>
  );
};

export default UpdateTopic;
