import { useEffect, useState } from "react";
import { Topic } from "../../../types/topics";
import { ProjectService } from "../../../firebase";
import { Link, useHistory, useParams } from "react-router-dom";
import { Chat } from "../../comments/chat/Chat";
import { NewComment } from "../../comments/newComment/NewComment";
import { User } from "../../../types/user";
import { Project } from "../../../types/projects";
import { Error } from "../../../types/error";
import store from "../../store/store";
import global from "../../../styles/global.module.scss";
import scss from "./topicdetail.module.scss";
import { ProjectIDParam, TopicIDParam } from "../../../types/paramObjs";

const TopicDetail = () => {
  const { pid }: ProjectIDParam = useParams();
  const { tid }: TopicIDParam = useParams();

  const history = useHistory();
  const [topic, setTopic] = useState<Topic | undefined>(undefined);
  const [project, setProject] = useState<Project | undefined>(undefined);
  const [errors, setErrors] = useState<Error[]>([]);

  const user: User = {
    uid: store.getState().user.uid,
    email: store.getState().user.email,
    displayName: store.getState().user.displayName
  };

  useEffect(() => {
    const project = ProjectService.getProjectById(pid).onSnapshot(
      (snapshot) => {
        if (snapshot.data()) {
          setProject(snapshot.data() as Project);
        } else {
          history.push("/404");
        }
      }
    );

    const topic = ProjectService.getProjectById(pid)
      .collection("topics")
      .doc(tid)
      .onSnapshot(
        (doc) => {
          if (doc.data()) {
            setTopic({
              id: doc.id,
              ...doc.data()
            } as Topic);
          } else {
            history.push("/404");
          }
        },
        () => {
          setErrors((errors) => [...errors, { msg: "Something went wrong!" }]);
        }
      );

    return () => {
      project();
      topic();
    };
  }, [pid, tid, history]);

  const resolveTopic = () => {
    clearErrors();
    if (topic && topic.user.uid === user.uid) {
      ProjectService.getProjectById(pid)
        .collection("topics")
        .doc(tid)
        .update({ resolved: true })
        .catch((error) => {
          setErrors((errors) => [...errors, { msg: "Something went wrong!" }]);
        });
    }
  };

  const clearErrors = () => {
    setErrors([]);
  };

  const addRequest = async () => {
    clearErrors();
    const docRef = ProjectService.getProjectById(pid)
      .collection("topics")
      .doc(tid);
    clearErrors();
    const doc = await docRef.get();

    if (doc.exists) {
      const topic = doc.data() as Topic;
      const isAlreadyRequester: boolean =
        topic.requests.find((req) => req.uid === store.getState().user.uid) !==
        undefined;
      const isAlreadyMember: boolean =
        topic.members.find((mem) => mem.uid === store.getState().user.uid) !==
        undefined;

      if (topic.user.uid !== user.uid) {
        if (isAlreadyRequester) {
          setErrors((errors) => [
            ...errors,
            { msg: "You already requested to participate in this project" }
          ]);
        } else if (isAlreadyMember) {
          setErrors((errors) => [
            ...errors,
            { msg: "You already a member of this project" }
          ]);
        } else {
          docRef
            .set({ requests: [...topic.requests, user] }, { merge: true })
            .catch(() => {
              setErrors((errors) => [
                ...errors,
                { msg: "Something went wrong!" }
              ]);
            });
        }
      } else {
        setErrors((errors) => [
          ...errors,
          { msg: "Owners can not ask for participation in their projects." }
        ]);
      }
    } else {
      setErrors((errors) => [...errors, { msg: "Something went wrong!" }]);
    }
  };

  const acceptRequest = async (request: User) => {
    clearErrors();

    const docRef = ProjectService.getProjectById(pid)
      .collection("topics")
      .doc(tid);

    const doc = await docRef.get();

    if (doc.exists) {
      const topic: Topic = doc.data() as Topic;
      const isRequester: boolean =
        topic.requests.find((req) => req.uid === request.uid) !== undefined;
      const isMember: boolean =
        topic.members.find((mem) => mem.uid === request.uid) !== undefined;

      if (topic.maxMembers > topic.members.length) {
        if (isRequester) {
          if (isMember) {
            setErrors((errors) => [
              ...errors,
              { msg: "Requester is already a member." }
            ]);
            topic.requests.splice(topic.requests.indexOf(request), 1);
          } else {
            topic.members.push(
              topic.requests.splice(topic.requests.indexOf(request), 1)[0]
            );
          }
          docRef.update(topic).catch(() => {
            setErrors((errors) => [
              ...errors,
              { msg: "Something went wrong!" }
            ]);
          });
        }
      } else {
        setErrors((errors) => [
          ...errors,
          { msg: "Maximum number of members already reached." }
        ]);
      }
    } else {
      setErrors((errors) => [...errors, { msg: "Something went wrong!" }]);
    }
  };

  const kickMember = async (member: User) => {
    clearErrors();
    const docRef = ProjectService.getProjectById(pid)
      .collection("topics")
      .doc(tid);

    const doc = await docRef.get();

    if (doc.exists) {
      const topic = doc.data() as Topic;
      const isMember =
        topic.members.find((mem) => mem.uid === member.uid) !== undefined;

      if (isMember) {
        topic.members.splice(topic.members.indexOf(member), 1);
        docRef.update(topic).catch(() => {
          setErrors((errors) => [...errors, { msg: "Something went wrong!" }]);
        });
      } else {
        setErrors((errors) => [...errors, { msg: "Something went wrong!" }]);
      }
    }
  };

  return (
    <section className={global.fullHeightCentered}>
      <section className={scss.titleBlock}>
        <Link to={"/project/" + pid} className={scss.projectInfo}>
          {project?.degreeCourse} | {project?.course} | {project?.name}
        </Link>
        <h1 className={global.titleBig}>{topic?.name}</h1>
        <div>{topic?.description}</div>
      </section>
      <section className={scss.memberInfos}>
        <article className={scss.infoCard}>
          <h3 className={scss.cardTitle}>
            Project Members ({topic?.members.length}/{topic?.maxMembers})
          </h3>
          <ul className={scss.unorderedList}>
            {topic?.members.map((member, index) => (
              <li key={index} className={scss.projectMember}>
                <div>{member.displayName}</div>
                {topic?.user.uid === user.uid &&
                  member.uid !== user.uid &&
                  !topic.resolved && (
                    <button
                      onClick={() => kickMember(member)}
                      className={`${global.buttonOrange} ${scss.removeButton}`}
                    >
                      Remove
                    </button>
                  )}
              </li>
            ))}
          </ul>
        </article>
        <article className={scss.infoCard}>
          <h3 className={scss.cardTitle}> Join Requests </h3>
          <ul className={scss.unorderedList}>
            {topic?.user.uid === user.uid && topic?.requests.length === 0 && (
              <li>No requests to show.</li>
            )}
            {topic?.requests.map((request, index) => (
              <li key={index} className={scss.projectMember}>
                <div>{request.displayName}</div>
                {topic?.user.uid === user.uid && !topic.resolved && (
                  <button
                    onClick={() => acceptRequest(request)}
                    className={`${global.buttonGreen} ${scss.acceptButton}`}
                  >
                    Accept
                  </button>
                )}
              </li>
            ))}
          </ul>
          {!(topic?.user.uid === user.uid) && !topic?.resolved && (
            <button
              onClick={addRequest}
              className={`${global.buttonGreen} ${scss.joinButton}`}
            >
              Send Join Request
            </button>
          )}
        </article>
      </section>
      {errors &&
        errors.map((error, index) => (
          <span key={index} className={global.errorMsg}>
            {error.msg}{" "}
          </span>
        ))}
      {topic?.resolved && (
        <span className={global.errorMsg}>Topic is already resolved!</span>
      )}
      {topic?.user.uid === user.uid && !topic.resolved && (
        <button onClick={resolveTopic} className={global.buttonGreen}>
          resolve Topic
        </button>
      )}
      {topic && (
        <section className={scss.commentSection}>
          <Chat topic={topic} />
          {!topic?.resolved && <NewComment />}
        </section>
      )}
    </section>
  );
};

export default TopicDetail;
