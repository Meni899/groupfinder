import { useState } from "react";
import { Topic } from "../../../types/topics";
import { ProjectService } from "../../../firebase";
import { Link, useParams } from "react-router-dom";
import { User } from "../../../types/user";
import { Error } from "../../../types/error";
import { useHistory } from "react-router-dom";
import store from "../../store/store";
import global from "../../../styles/global.module.scss";
import scss from "./createtopic.module.scss";
import { ProjectIDParam } from "../../../types/paramObjs";

const CreateTopic = () => {
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [maxMembers, setMaxMembers] = useState(2);
  const [errors, setErrors] = useState<Error[]>([]);

  const { pid }: ProjectIDParam = useParams();

  const history = useHistory();

  const user: User = {
    uid: store.getState().user.uid,
    email: store.getState().user.email,
    displayName: store.getState().user.displayName,
  };

  const clearErrors = () => {
    setErrors([]);
  };

  const handleCreateNewTopic = () => {
    clearErrors();
    if (name === "") {
      setErrors((errors) => [...errors, { msg: "Name is required." }]);
    } else if (description === "") {
      setErrors((errors) => [...errors, { msg: "Description is required." }]);
    } else if (maxMembers < 0) {
      setErrors((errors) => [
        ...errors,
        { msg: "Maximum number of members has to be 0 or greater." },
      ]);
    } else {
      const topic: Topic = {
        name: name,
        description: description,
        user: user,
        maxMembers: maxMembers,
        members: [user],
        requests: [],
        resolved: false,
        timestamp: new Date(),
      };

      ProjectService.getProjectById(pid)
        .collection("topics")
        .add(topic)
        .then(() => {
          history.push("/project/" + pid);
        })
        .catch((error) => {
          console.error("Error adding document: ", error);
        });
    }
  };

  return (
    <section className={global.fullHeightCentered}>
      <h1 className={global.titleBig}>Create a new Topic</h1>
      <div className={global.errorMsg}>
        {errors.map((error, index) => (
          <span key={index}>{error.msg} </span>
        ))}
      </div>
      <input
        type='text'
        autoFocus
        required
        value={name}
        onChange={(e) => setName(e.target.value)}
        className={global.inputfield}
        placeholder='Topic Name'
        aria-label='Topic Name'
        data-testid='topicName'
      />
      <input
        type='text'
        autoFocus
        required
        value={description}
        onChange={(e) => setDescription(e.target.value)}
        className={global.inputfield}
        placeholder='Description'
        aria-label='Description'
        data-testid='topicDescription'
      />
      <input
        type='number'
        autoFocus
        required
        value={maxMembers}
        min='2'
        onChange={(e) => setMaxMembers(e.target.valueAsNumber)}
        className={global.inputfield}
        placeholder='Max. Group Members'
        aria-label='Max. Group Members'
        data-testid='maxMembers'
      />

      <button
        onClick={handleCreateNewTopic}
        className={`${global.buttonGreen} ${scss.submitButton}`}
        data-testid='createButton'
      >
        Create Topic
      </button>
      <Link to={"/project/" + pid}>
        <button className={`${global.buttonOrange} ${scss.backButton}`}>
          Cancel
        </button>
      </Link>
    </section>
  );
};

export default CreateTopic;
