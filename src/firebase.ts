import firebase from "firebase/app";
import "firebase/database";
import "firebase/firestore";
import { Project } from "./types/projects";

// Your web app's Firebase configuration
let firebaseConfig = {
  apiKey: "AIzaSyBOTbP4zPolm7GwJavmNgXhIUUKJuGaSOg",
  authDomain: "groupfinder-67d32.firebaseapp.com",
  projectId: "groupfinder-67d32",
  storageBucket: "groupfinder-67d32.appspot.com",
  messagingSenderId: "1095385163177",
  appId: "1:1095385163177:web:e1d7634fcbc407571d90b8",
};

// Initialize Firebase
const Firebase = firebase.initializeApp(firebaseConfig);
const auth = firebase.auth();
const firestore = firebase.firestore();

//basic operations
const getProjects = () => {
  return firestore.collection("projects");
};

const getProjectById = (id: string) => {
  return firestore.collection("projects").doc(id);
};

const createProject = (project: Project) => {
  return firestore.collection("projects").add(project);
};

const removeProject = (id: string) => {
  return firestore.collection("projects").doc(id).delete();
};

const ProjectService = {
  getProjects,
  getProjectById,
  createProject,
  removeProject,
};

export default Firebase;

export { auth, firestore, firebase, ProjectService };
