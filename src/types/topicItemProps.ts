import { Topic } from "./topics";

export type TopicItemProps = {
  pid: any;
  topic: Topic;
};
