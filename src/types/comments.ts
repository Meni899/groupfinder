import { User } from "./user";

export type Comment = {
  id?: string;
  message: string;
  user: User;
  likes: User[];
  timestamp: Date;
};
